package nuthatch.java_ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class CompileUnit extends JavaAstNode {
    private TreeSet<Type> imports = new TreeSet<>();

    public CompileUnit(Type[] importTypes, Class[] classes) {
        if (importTypes != null)
            Collections.addAll(imports, importTypes);
        if (classes != null)
            Collections.addAll(children, classes);
    }

    public void addImport(Type importType) {
        if (importType.getNamespace() != null)
            imports.add(importType);
    }

    @Override
    public List<JavaAstNode> getChildren() {
        List<JavaAstNode> combinedChildren = new ArrayList<>(imports);
        combinedChildren.addAll(children);
        return combinedChildren;
    }

    public List<Type> getImports() {
        return new ArrayList<>(imports);
    }

    public List<Class> getClasses() {
        return children.stream()
                .filter(c -> c instanceof Class)
                .map(c -> (Class) c)
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompileUnit)) return false;
        if (!super.equals(o)) return false;

        CompileUnit that = (CompileUnit) o;

        return imports.equals(that.imports);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + imports.hashCode();
        return result;
    }
}
