grammar Nuthatch;
walk : 'walk' closure;

closure : ID paras? '{' ('state' declaration)* statement+ '}' ;

paras : '(' ID (',' ID)* ')' ;

statement : '{' statement+ '}'                                  # CompoundStatement
        | 'if' expression 'then' statement ('else' statement)?  # IfStatement
        | declaration                                           # DeclarationStatement
        | ID '=' expression ';'                                 # AssignStatement
        | expression ';'                                        # ExpressionStatement
        | 'return' expression ';'                               # ReturnStatement
        | 'walk' 'to' expression ';'                            # WalkToStatement
        | 'stop' ';'                                            # StopStatement
        | 'suspend' ';'                                         # SuspendStatement
        | '!' term ';'                                          # ReplaceStatement
        | function                                              # FunctionStatement
        | action                                                # ActionStatement
        ;

declaration : type? ID '=' expression ';' ;

// Should have Java style expressions as well
expression : branch            # BranchExpression
           | joinpoint         # JoinPointExpression
           | getter            # GetterExpression
           | '~' ID arguments? # NestedWalkExpression
           | literal           # LiteralExpression
           | ID                # RefExpression
           ;

literal : INT    # IntLiteral
        | STRING # StringLiteral
        ;

branch : 'parent'  # ParentBranch
       | 'first'   # FirstBranch
       | 'last'    # LastBranch
       | 'next'    # NextBranch
       | 'proceed' # ProceedBranch
       | 'stay'    # StayBranch
       ;

joinpoint : 'up'            # UpJoinPoint
          | 'down'          # DownJoinPoint
          | 'root'          # RootJoinPoint
          | 'leaf'          # LeafJoinPoint
          | '?' term        # MatchAndBindJoinPoint
          | 'before' getter # BeforeJoinPoint
          | 'after' getter  # AfterJoinPoint
          ;

action : 'action' closure ;

function : 'function' closure ;

type : 'integer'
     | 'string'
     ;

term : ID arguments? # RefTerm
     | literal       # LiteralTerm
     ;

arguments : '()'
          | '(' expression (',' expression)* ')' ;

getter : 'arity'                # ArityGetter
       | 'root'                 # RootGetter
       | 'leaf'                 # LeafGetter
       | 'name'                 # NameGetter
       | 'type'                 # TypeGetter
       | 'data'                 # DataGetter
       | 'child' '[' INT ']' # ChildGetter
       | 'label' '[' INT ']' # LabelGetter
       ;

ID : [A-Za-z]+ ;
INT : [0-9]+ ;
STRING : '"' ('\\"'|.)*? '"' ;
WS : [ \t\r\n]+ -> skip ;