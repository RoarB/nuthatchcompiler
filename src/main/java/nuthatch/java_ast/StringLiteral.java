package nuthatch.java_ast;

public class StringLiteral extends JavaAstNode implements Expr {
    private String value;

    public StringLiteral(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StringLiteral)) return false;
        if (!super.equals(o)) return false;

        StringLiteral that = (StringLiteral) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    public String toString() {
        return "StringLiteral(\"" + value + "\")";
    }
}
