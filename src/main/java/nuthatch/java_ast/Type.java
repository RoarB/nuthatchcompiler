package nuthatch.java_ast;

public class Type extends JavaAstNode implements Comparable<Type> {
    private final String namespace;
    private final String name;

    public Type(String namespace, String name) {
        this.namespace = namespace;
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        if (namespace != null)
            return namespace + "." + name;
        else
            return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Type type = (Type) o;

        if (namespace != null ? !namespace.equals(type.namespace) : type.namespace != null) return false;
        return name.equals(type.name);
    }

    @Override
    public int hashCode() {
        int result = namespace != null ? namespace.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public int compareTo(Type o) {
        return getFullName().compareTo(o.getFullName());
    }
}
