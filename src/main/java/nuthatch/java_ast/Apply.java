package nuthatch.java_ast;

import java.util.List;
import java.util.stream.Collectors;

public class Apply extends JavaAstNode implements Expr, Statement {
    public Apply(Ref object, Id methodName, Expr... args) {
        addChild(object);
        addChild(methodName);
        for (Expr expr : args)
            addChild(expr);
    }

    public Ref getObject() { return (Ref)children.get(0); }

    public Id getMethodName() { return (Id)children.get(1); }

    public List<Expr> getArguments() {
        return children.subList(2, children.size()).stream()
                .map(e -> (Expr) e)
                .collect(Collectors.toList());
    }

    public String toString() {
        return "Apply(" + getObject() + ", " + getMethodName().toString() + ", " + getArguments().toString() + ")";
    }
}
