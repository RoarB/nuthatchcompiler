package nuthatch.java_ast;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Base class for a node in the AST
 */
public abstract class JavaAstNode implements AstNode {
    protected List<JavaAstNode> children = new ArrayList<>();

    public void addChild(AstNode newChild) {
        children.add((JavaAstNode)newChild);
    }

    public List<JavaAstNode> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JavaAstNode)) return false;

        JavaAstNode that = (JavaAstNode) o;

        return children.equals(that.children);
    }

    @Override
    public int hashCode() {
        return children.hashCode();
    }
}
