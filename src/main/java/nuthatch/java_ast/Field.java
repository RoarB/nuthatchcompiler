package nuthatch.java_ast;

public class Field extends JavaAstNode implements Statement {
    public Field(Modifier modifier, Type typ, Id name, Expr valueExpr) {
        addChild(modifier);
        addChild(typ);
        addChild(name);
        addChild(valueExpr);
    }

    public Modifier getModifier() {
        return (Modifier)children.get(0);
    }

    public Type getType() {
        return (Type)children.get(1);
    }

    public Id getName() { return (Id)children.get(2); }

    public Expr getValueExpr() {
        return (Expr)children.get(3);
    }

}
