package nuthatch.java_ast;

public class Add extends JavaAstNode implements Expr {
    public Add(Expr e1, Expr e2) {
        addChild(e1);
        addChild(e2);
    }
}
