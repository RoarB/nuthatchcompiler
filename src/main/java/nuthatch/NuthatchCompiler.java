package nuthatch;

import nuthatch.java_ast.CompileUnit;
import org.antlr.v4.runtime.*;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class NuthatchCompiler {
    public static void main(String[] args) throws IOException {
        if (args.length == 0 ){
            System.out.print("Usage: NuthatchCompiler <filename.nut>");
            return;
        }

        String nuthatchWalk = new String(Files.readAllBytes(Paths.get(args[0])));

        CharStream inputCharStream = new ANTLRInputStream(new StringReader(nuthatchWalk));
        TokenSource tokenSource = new NuthatchLexer(inputCharStream);
        TokenStream inputTokenStream = new CommonTokenStream(tokenSource);
        NuthatchParser parser = new NuthatchParser(inputTokenStream);

        NuthatchParser.WalkContext context = parser.walk();
        NuthatchToJavaAstVisitor visitor = new NuthatchToJavaAstVisitor();
        CompileUnit unit = (CompileUnit)visitor.visit(context);

        System.out.print(JavaAstPrettyPrinter.prettyPrint(unit));
    }
}
