package nuthatch.java_ast;

public class Annotation extends JavaAstNode {
    private final String annotationText;

    public Annotation(String annotationText) {
        this.annotationText = annotationText;
    }

    public String getAnnotationText() {
        return annotationText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Annotation)) return false;
        if (!super.equals(o)) return false;

        Annotation that = (Annotation) o;

        return annotationText != null ? annotationText.equals(that.annotationText) : that.annotationText == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (annotationText != null ? annotationText.hashCode() : 0);
        return result;
    }
}
