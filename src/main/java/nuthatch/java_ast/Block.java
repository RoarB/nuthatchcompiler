package nuthatch.java_ast;

public class Block extends JavaAstNode implements Statement {
    public Block(Statement... statements) {
        for (Statement statement : statements)
            addChild(statement);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Block(\n");
        for (JavaAstNode node : getChildren())
            sb.append("  ").append(node.toString()).append("\n");
        return sb.append(")").toString();
    }
}
