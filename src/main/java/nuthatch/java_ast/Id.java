package nuthatch.java_ast;

public class Id extends JavaAstNode {
    private final String value;

    public Id(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Id)) return false;
        if (!super.equals(o)) return false;

        Id id = (Id) o;

        return value.equals(id.value);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    public String toString() {
        return "Id(\"" + value + "\")";
    }
}
