package nuthatch.java_ast;

public class BoolLiteral extends JavaAstNode implements Expr {
    private Boolean value;

    public BoolLiteral(Boolean value) {
        this.value = value;
    }

    public Boolean getValue() { return value; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BoolLiteral)) return false;
        if (!super.equals(o)) return false;

        BoolLiteral boolLiteral = (BoolLiteral) o;

        return value.equals(boolLiteral.value);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
