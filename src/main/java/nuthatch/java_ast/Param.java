package nuthatch.java_ast;

public class Param extends JavaAstNode {
    public Param(Type typ, Id name) {
        addChild(typ);
        addChild(name);
    }

    public Type getType() { return (Type)children.get(0); }

    public Id getName() { return (Id)children.get(1); }
}
