package nuthatch.java_ast;

public class Mul extends JavaAstNode implements Expr {
    public Mul(Expr e1, Expr e2) {
        addChild(e1);
        addChild(e2);
    }
}
