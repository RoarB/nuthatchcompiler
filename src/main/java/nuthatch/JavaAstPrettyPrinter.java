package nuthatch;

import nuthatch.java_ast.*;
import nuthatch.java_ast.Class;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JavaAstPrettyPrinter {
    public static String prettyPrint(CompileUnit unit) {
        StringBuilder sb = new StringBuilder();
        List<Class> classes = new ArrayList<>();

        // Print the imports, save classes for later
        for (JavaAstNode node : unit.getChildren()) {
            if (node instanceof Type)
                print(sb, 0, (Type)node);
            if (node instanceof Class)
                classes.add((Class)node);
        }

        addLine(sb, 0, "");

        // Print the classes
        for (Class cls : classes) {
            print(sb, 0, cls);
        }

        return sb.toString();
    }

    private static void print(StringBuilder sb, Integer indentLevel, Type typ) {
        addLine(sb, indentLevel, "import " + typ.getFullName() + ";");
    }

    private static void print(StringBuilder sb, Integer indentLevel, Class cls) {
        // First print the class declaration
        String classDecl = cls.getModifier().getValue() + " class " + cls.getName().getValue();
        Id classExtends = cls.getExtends();
        Id classImplements = cls.getImplements();
        if (classExtends != null)
            classDecl += " extends " + classExtends.getValue();
        if (classImplements != null)
            classDecl += " implements " + classImplements.getValue();
        addLine(sb, indentLevel, classDecl + " {");

        // Print the fields, save the methods for later
        List<Method> methods = new ArrayList<>();
        for (JavaAstNode node : cls.getChildren()) {
            if (node instanceof Field)
                print(sb, indentLevel + 1, (Field)node);
            if (node instanceof Method)
                methods.add((Method)node);
        }
        addLine(sb, 0, "");

        // Print the methods
        for (Method method : methods) {
            print(sb, indentLevel + 1, method);
        }

        addLine(sb, indentLevel, "}");
    }

    private static void print(StringBuilder sb, Integer indentLevel, Field field) {
        addLine(sb, indentLevel, field.getType().getName() + " "
                + field.getName() + " = " + exprToString(field.getValueExpr()));
    }

    private static void print(StringBuilder sb, Integer indentLevel, Method method) {
        String paramStr = method.getParams().stream()
                .map(param -> param.getType().getName() + " " + param.getName().getValue())
                .collect(Collectors.joining(", "));
        Annotation annotation = method.getAnnotation();

        // Print annotation and declaration
        if (annotation != null)
            addLine(sb, indentLevel, annotation.getAnnotationText());
        addLine(sb, indentLevel, method.getModifier().getValue() + " " + method.getReturnType().getName()
                + " " + method.getName().getValue() + "(" + paramStr + ") {");

        // Print all the statements
        for (Statement statement : method.getStatements()) {
            print(sb, indentLevel + 1, statement);
        }

        addLine(sb, indentLevel, "}");
    }

    private static void print(StringBuilder sb, Integer indentLevel, Statement statement) {
        if (statement instanceof Return)
            print(sb, indentLevel, (Return)statement);
        else if (statement instanceof If)
            print(sb, indentLevel, (If)statement);
        else if (statement instanceof Block)
            print(sb, indentLevel, (Block)statement);
        else if (statement instanceof Apply)
            print(sb, indentLevel, (Apply)statement);
    }

    private static void print(StringBuilder sb, Integer indentLevel, Return returnStatement) {
        Expr expr = (Expr)returnStatement.getChildren().get(0);
        String returnString = "return";
        if (expr != null)
            returnString += " " + exprToString(expr);
        addLine(sb, indentLevel, returnString + ";");
    }

    private static void print(StringBuilder sb, Integer indentLevel, If ifStatement) {
        String ifLine = "if (" + exprToString(ifStatement.getCondition()) + ")";
        addLine(sb, indentLevel, ifLine);
        print(sb, indentLevel + 1, ifStatement.getThenStatement());
        if (ifStatement.getElseStatement() != null) {
            addLine(sb, indentLevel, "else");
            print(sb, indentLevel + 1, ifStatement.getElseStatement());
        }
    }

    private static void print(StringBuilder sb, Integer indentLevel, Block blockStat) {
        addLine(sb, indentLevel, "{");
        for (JavaAstNode node : blockStat.getChildren())
            print(sb, indentLevel + 1, (Statement)node);
        addLine(sb, indentLevel, "}");
    }

    private static void print(StringBuilder sb, Integer indentLevel, Apply applyStat) {
        addLine(sb, indentLevel, exprToString(applyStat) + ";");
    }

    private static String exprToString(Expr e) {
        if (e instanceof StringLiteral)
            return exprToString((StringLiteral)e);
        if (e instanceof IntLiteral)
            return exprToString((IntLiteral)e);
        if (e instanceof Ref)
            return exprToString((Ref)e);
        if (e instanceof Apply)
            return exprToString((Apply)e);
        return "";
    }

    private static String exprToString(StringLiteral e) {
        return "\"" + e.getValue() + "\"";
    }

    private static String exprToString(IntLiteral e) {
        return e.getValue().toString();
    }

    private static String exprToString(Ref e) { return e.getId().getValue(); }

    private static String exprToString(Apply e) {
        StringBuilder builder = new StringBuilder();

        // Method name (with optional object in front of it)
        Ref obj = e.getObject();
        if (obj != null)
            builder.append(obj.getId().getValue() + ".");
        builder.append(e.getMethodName().getValue() + "(");

        // Arguments
        List<Expr> args = e.getArguments();
        String[] argStrings = new String[args.size()];
        int i = 0;
        for (Expr arg : args)
            argStrings[i++] = exprToString(arg);
        builder.append(String.join(", ", argStrings)).append(")");

        return builder.toString();
    }

    private static void addLine(StringBuilder sb, Integer indentLevel, String text) {
        for (int i = 0; i < indentLevel; i++)
            sb.append("    ");
        sb.append(text).append("\n");
    }
}
