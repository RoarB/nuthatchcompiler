package nuthatch.java_ast;

public class Modifier extends JavaAstNode {
    private final String value;

    public Modifier(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Modifier)) return false;
        if (!super.equals(o)) return false;

        Modifier modifier = (Modifier) o;

        return value.equals(modifier.value);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
