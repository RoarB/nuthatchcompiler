package nuthatch.java_ast;

/**
 * Interface used for marking a node type as a statement
 */
public interface Statement extends AstNode{

}
