# README #

Nuthatch to Java compiler.

## Compiling ##
Download and run "mvn clean package" to build.

## Running ##
"java -jar target/nuthatch-compiler-0.1.jar examples/simplify.nut"