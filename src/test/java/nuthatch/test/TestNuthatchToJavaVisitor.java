package nuthatch.test;

import junit.framework.TestCase;
import nuthatch.NuthatchLexer;
import nuthatch.NuthatchParser;
import nuthatch.NuthatchToJavaAstVisitor;
import nuthatch.java_ast.Class;
import nuthatch.java_ast.*;
import org.antlr.v4.runtime.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class TestNuthatchToJavaVisitor extends TestCase {

    public final void testDefaultWalk() {
        String nuthatchWalk = "walk default { walk to next; }";
        CompileUnit unit = translateWalk(nuthatchWalk);
        List<Statement> expectedStatements = new ArrayList<>();
        expectedStatements.add(new Return(new Ref(new Id("NEXT"))));
        checkJavaAst("Default", expectedStatements, unit);
    }

    public final void testSimplify() {
        String nuthatchWalk =
                "walk simplify {\n" +
                "  if up then {\n" +
                "    if ?Add(x, 0) then !x;\n" +
                "    if ?Mul(x, 0) then !0;\n" +
                "  }\n" +
                "  walk to next;\n" +
                "}";
        CompileUnit unit = translateWalk(nuthatchWalk);

        List<Statement> expectedStatements = new ArrayList<>();
        Statement outerIfStatement =
            new If(new Apply(null, new Id("up"), new Ref(new Id("w"))),
                new Block(
                        new If(new Apply(new Ref(new Id("w")), new Id("match"), new Apply(null, new Id("Add"), new Apply(null, new Id("var"), new StringLiteral("x")), new IntLiteral(0))),
                            new Apply(new Ref(new Id("w")), new Id("replace"), new Apply(null, new Id("var"), new StringLiteral("x"))),
                            null),
                        new If(new Apply(new Ref(new Id("w")), new Id("match"), new Apply(null, new Id("Mul"), new Apply(null, new Id("var"), new StringLiteral("x")), new IntLiteral(0))),
                            new Apply(new Ref(new Id("w")), new Id("replace"), new IntLiteral(0)),
                            null)
                ), null);
        expectedStatements.add(outerIfStatement);
        expectedStatements.add(new Return(new Ref(new Id("NEXT"))));

        checkJavaAst("Simplify", expectedStatements, unit);
    }

    private CompileUnit translateWalk(String nuthatchWalk) {
        CharStream inputCharStream = null;
        try {
            inputCharStream = new ANTLRInputStream(new StringReader(nuthatchWalk));
        } catch (IOException e) {
            e.printStackTrace();
        }
        TokenSource tokenSource = new NuthatchLexer(inputCharStream);
        TokenStream inputTokenStream = new CommonTokenStream(tokenSource);
        NuthatchParser parser = new NuthatchParser(inputTokenStream);

        NuthatchParser.WalkContext context = parser.walk();

        NuthatchToJavaAstVisitor visitor = new NuthatchToJavaAstVisitor();
        return (CompileUnit)visitor.visit(context);
    }

    private void checkJavaAst(String expectedClassName, List<Statement> expectedStatements, CompileUnit actualUnit) {
        // Check the generated compile unit. First check all the stuff that are common, then check the statements
        // in the "step" method against the given list of statements

        // Check import statements
        List<Type> expectedImports = new ArrayList<>();
        expectedImports.add(new Type("nuthatch.examples.xmpllang.expronly", "ExprWalker"));
        expectedImports.add(new Type("nuthatch.library", "BaseWalk"));
        assertEquals(expectedImports, actualUnit.getImports());

        // Check class
        List<Class> actualClasses = actualUnit.getClasses();
        assertEquals(1, actualClasses.size());
        Class workerClass = actualClasses.get(0);
        assertEquals("public", workerClass.getModifier().getValue());
        assertEquals(expectedClassName, workerClass.getName().getValue());
        assertEquals("BaseWalk<ExprWalker>", workerClass.getExtends().getValue());
        assertEquals(null, workerClass.getImplements());

        // Check method in class
        List<Method> actualMethods = workerClass.getMethods();
        assertEquals(1, actualMethods.size());
        Method stepMethod = actualMethods.get(0);
        assertEquals("@Override", stepMethod.getAnnotation().getAnnotationText());
        assertEquals("public", stepMethod.getModifier().getValue());
        assertEquals("int", stepMethod.getReturnType().getName());
        assertEquals("step", stepMethod.getName().getValue());
        List<Param> stepParams = stepMethod.getParams();
        assertEquals(1, stepParams.size());
        Param stepParam = stepParams.get(0);
        assertEquals("ExprWalker", stepParam.getType().getName());
        assertEquals("w", stepParam.getName().getValue());

        // Check the statements in the method
        assertEquals(stepMethod.getStatements(), expectedStatements);
    }
}