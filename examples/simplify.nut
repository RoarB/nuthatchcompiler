walk simplify {
  if up then {
    if ?Add(x, 0) then !x;
    if ?Mul(x, 0) then !0;
  }
  walk to next;
}
