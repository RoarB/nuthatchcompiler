package nuthatch.java_ast;

public class Var extends JavaAstNode {
    public Var(Type typ, Id name, Expr value) {
        addChild(typ);
        addChild(name);
        addChild(value);
    }

    public Type getType() { return (Type)children.get(0); }

    public Id getName() {
        return (Id)children.get(1);
    }

    public Expr getValue() { return (Expr)children.get(2); }
}
