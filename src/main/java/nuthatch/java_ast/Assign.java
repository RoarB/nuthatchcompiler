package nuthatch.java_ast;

public class Assign extends JavaAstNode implements Statement {
    public Assign(Var var, Expr expr) {
        addChild(var);
        addChild(expr);
    }
}
