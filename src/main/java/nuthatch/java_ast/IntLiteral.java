package nuthatch.java_ast;

public class IntLiteral extends JavaAstNode implements Expr {
    Integer value;

    public IntLiteral(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntLiteral)) return false;
        if (!super.equals(o)) return false;

        IntLiteral intLiteral = (IntLiteral) o;

        return value.equals(intLiteral.value);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    public String toString() {
        return "IntLiteral(" + value + ")";
    }
}
