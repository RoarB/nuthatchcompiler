package nuthatch.java_ast;

public class Ref extends JavaAstNode implements Expr {
    public Ref(Id id) {
        addChild(id);
    }

    public Id getId() { return (Id)children.get(0); }

    public String toString() {
        return "Ref(" + getId().toString() + ")";
    }
}
