package nuthatch;

import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

public class NuthatchToJavaVisitor extends AbstractParseTreeVisitor<String> implements NuthatchVisitor<String> {
    private StringBuilder importStatements = new StringBuilder();
    //private StringBuilder body = new StringBuilder();

    public String visitWalk(NuthatchParser.WalkContext ctx) {
        String closureString = visit(ctx.closure());
        return importStatements.toString() + "\n" + closureString;
    }

    public String visitClosure(NuthatchParser.ClosureContext ctx) {
        addImportStatement("nuthatch.examples.xmpllang.expronly.ExprWalker");
        addImportStatement("nuthatch.library.BaseWalk");
        StringBuilder classDef = new StringBuilder();

        addLine(classDef, 0, "public class " + ctx.ID().getText() + " extends BaseWalk<ExprWalker> {");

        // State declarations as member variables
        for (NuthatchParser.DeclarationContext declCtx : ctx.declaration()) {
            addLine(classDef, 1, visit(declCtx));
        }

        // The step method
        addLine(classDef, 1,"@Override");
        addLine(classDef, 1,"public int step(ExprWalker w) {");
        for (NuthatchParser.StatementContext stmtCtx : ctx.statement()) {
            addLine(classDef, 2, visit(stmtCtx));
        }
        addLine(classDef, 1,"}");

        // End of class
        addLine(classDef, 0,"}");

        return classDef.toString();
    }

    public String visitParas(NuthatchParser.ParasContext ctx) {
        return null;
    }

    public String visitCompoundStatement(NuthatchParser.CompoundStatementContext ctx) {
        return null;
    }

    public String visitIfStatement(NuthatchParser.IfStatementContext ctx) {
        return null;
    }

    public String visitDeclarationStatement(NuthatchParser.DeclarationStatementContext ctx) {
        return null;
    }

    public String visitAssignStatement(NuthatchParser.AssignStatementContext ctx) {
        return null;
    }

    public String visitExpressionStatement(NuthatchParser.ExpressionStatementContext ctx) {
        return null;
    }

    public String visitReturnStatement(NuthatchParser.ReturnStatementContext ctx) {
        return null;
    }

    public String visitWalkToStatement(NuthatchParser.WalkToStatementContext ctx) {
        return "return " + visit(ctx.expression()) + ";\n";
    }

    public String visitStopStatement(NuthatchParser.StopStatementContext ctx) {
        return null;
    }

    public String visitSuspendStatement(NuthatchParser.SuspendStatementContext ctx) {
        return null;
    }

    public String visitFunctionStatement(NuthatchParser.FunctionStatementContext ctx) {
        return null;
    }

    public String visitActionStatement(NuthatchParser.ActionStatementContext ctx) {
        return null;
    }

    public String visitReplaceStatement(NuthatchParser.ReplaceStatementContext ctx) {
        return null;
    }

    public String visitDeclaration(NuthatchParser.DeclarationContext ctx) {
        return null;
    }

    public String visitBranchExpression(NuthatchParser.BranchExpressionContext ctx) {
        return visit(ctx.branch());
    }

    public String visitJoinPointExpression(NuthatchParser.JoinPointExpressionContext ctx) {
        return null;
    }

    public String visitGetterExpression(NuthatchParser.GetterExpressionContext ctx) {
        return null;
    }

    public String visitNestedWalkExpression(NuthatchParser.NestedWalkExpressionContext ctx) {
        return null;
    }

    @Override
    public String visitLiteralExpression(NuthatchParser.LiteralExpressionContext ctx) {
        return null;
    }

    @Override
    public String visitRefExpression(NuthatchParser.RefExpressionContext ctx) {
        return null;
    }

    @Override
    public String visitIntLiteral(NuthatchParser.IntLiteralContext ctx) {
        return null;
    }

    @Override
    public String visitStringLiteral(NuthatchParser.StringLiteralContext ctx) {
        return null;
    }

    public String visitParentBranch(NuthatchParser.ParentBranchContext ctx) {
        return null;
    }

    public String visitFirstBranch(NuthatchParser.FirstBranchContext ctx) {
        return null;
    }

    public String visitLastBranch(NuthatchParser.LastBranchContext ctx) {
        return null;
    }

    public String visitNextBranch(NuthatchParser.NextBranchContext ctx) {
        return "NEXT";
    }

    public String visitProceedBranch(NuthatchParser.ProceedBranchContext ctx) {
        return null;
    }

    public String visitStayBranch(NuthatchParser.StayBranchContext ctx) {
        return null;
    }

    public String visitUpJoinPoint(NuthatchParser.UpJoinPointContext ctx) {
        return null;
    }

    public String visitDownJoinPoint(NuthatchParser.DownJoinPointContext ctx) {
        return null;
    }

    public String visitRootJoinPoint(NuthatchParser.RootJoinPointContext ctx) {
        return null;
    }

    public String visitLeafJoinPoint(NuthatchParser.LeafJoinPointContext ctx) {
        return null;
    }

    public String visitMatchAndBindJoinPoint(NuthatchParser.MatchAndBindJoinPointContext ctx) {
        return null;
    }

    public String visitBeforeJoinPoint(NuthatchParser.BeforeJoinPointContext ctx) {
        return null;
    }

    public String visitAfterJoinPoint(NuthatchParser.AfterJoinPointContext ctx) {
        return null;
    }

    public String visitAction(NuthatchParser.ActionContext ctx) {
        return null;
    }

    public String visitFunction(NuthatchParser.FunctionContext ctx) {
        return null;
    }

    public String visitType(NuthatchParser.TypeContext ctx) {
        return null;
    }

    @Override
    public String visitRefTerm(NuthatchParser.RefTermContext ctx) {
        return null;
    }

    @Override
    public String visitLiteralTerm(NuthatchParser.LiteralTermContext ctx) {
        return null;
    }

    public String visitTerm(NuthatchParser.TermContext ctx) {
        return null;
    }

    public String visitArguments(NuthatchParser.ArgumentsContext ctx) {
        return null;
    }

    public String visitArityGetter(NuthatchParser.ArityGetterContext ctx) {
        return null;
    }

    public String visitRootGetter(NuthatchParser.RootGetterContext ctx) {
        return null;
    }

    public String visitLeafGetter(NuthatchParser.LeafGetterContext ctx) {
        return null;
    }

    public String visitNameGetter(NuthatchParser.NameGetterContext ctx) {
        return null;
    }

    public String visitTypeGetter(NuthatchParser.TypeGetterContext ctx) {
        return null;
    }

    public String visitDataGetter(NuthatchParser.DataGetterContext ctx) {
        return null;
    }

    public String visitChildGetter(NuthatchParser.ChildGetterContext ctx) {
        return null;
    }

    public String visitLabelGetter(NuthatchParser.LabelGetterContext ctx) {
        return null;
    }

    private void addImportStatement(String importPath) {
        importStatements.append("import ").append(importPath).append(";\n");
    }

    private void addLine(StringBuilder sb, Integer indentLevel, String text) {
        String indent = "    ";
        for (int i = 0; i < indentLevel; i++)
            sb.append(indent);
        sb.append(text).append("\n");
    }
}
