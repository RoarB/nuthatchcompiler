package nuthatch.java_ast;

import java.util.List;
import java.util.stream.Collectors;

public class Method extends JavaAstNode {
    public Method(Annotation annotation, Modifier modifier, Type returnType, Id name,
                  List<Param> params, Statement... statements) {
        addChild(annotation);
        addChild(modifier);
        addChild(returnType);
        addChild(name);
        children.addAll(params);
        for (Statement statement : statements)
            addChild(statement);
    }

    public Annotation getAnnotation() {
        return (Annotation)children.get(0);
    }

    public Modifier getModifier() { return (Modifier)children.get(1); }

    public Type getReturnType() {
        return (Type) children.get(2);
    }

    public Id getName() {
        return (Id) children.get(3);
    }

    public List<Param> getParams() {
        return children.stream()
                .filter(p -> p instanceof Param)
                .map(p -> (Param) p)
                .collect(Collectors.toList());
    }

    public List<Statement> getStatements() {
        return children.stream()
                .filter(p -> p instanceof Statement)
                .map(p -> (Statement) p)
                .collect(Collectors.toList());
    }
}
