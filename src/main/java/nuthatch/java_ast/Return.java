package nuthatch.java_ast;

public class Return extends JavaAstNode implements Statement {
    public Return(Expr e) {
        addChild(e);
    }

    public Expr getExpression() { return (Expr)children.get(0); }
}
