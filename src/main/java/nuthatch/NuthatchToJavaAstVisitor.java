package nuthatch;

import nuthatch.java_ast.*;
import nuthatch.java_ast.Class;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NuthatchToJavaAstVisitor extends AbstractParseTreeVisitor<JavaAstNode> implements NuthatchVisitor<JavaAstNode> {
    private CompileUnit unit = new CompileUnit(null, null);
    private static final String WALKER_PARAM_NAME = "w";

    public JavaAstNode visitWalk(NuthatchParser.WalkContext ctx) {
        unit.addChild(visit(ctx.closure()));
        return unit;
    }

    public JavaAstNode visitClosure(NuthatchParser.ClosureContext ctx) {
        unit.addImport(new Type("nuthatch.examples.xmpllang.expronly", "ExprWalker"));
        unit.addImport(new Type("nuthatch.library", "BaseWalk"));
        Class walkerClass = new Class(new Modifier("public"), new Id(capitalize(ctx.ID().getText())),
                new Id("BaseWalk<ExprWalker>"), null);

        // State declarations as member variables
        for (NuthatchParser.DeclarationContext declCtx : ctx.declaration()) {
            walkerClass.addChild(visit(declCtx));
        }

        // The step method
        Method method = new Method(new Annotation("@Override"), new Modifier("public"), new Type(null, "int"), new Id("step"),
                Collections.singletonList(new Param(new Type("nuthatch.examples.xmpllang.expronly", "ExprWalker"), new Id(WALKER_PARAM_NAME))));
        for (NuthatchParser.StatementContext stmtCtx : ctx.statement()) {
            method.addChild(visit(stmtCtx));
        }
        walkerClass.addChild(method);

        return walkerClass;
    }

    public JavaAstNode visitParas(NuthatchParser.ParasContext ctx) {
        Params params = new Params();
        for (TerminalNode param : ctx.ID())
            params.addChild(new Param(null, new Id(param.getText()))); // We don't know the type of the parameters
        return params;
    }

    public JavaAstNode visitCompoundStatement(NuthatchParser.CompoundStatementContext ctx) {
        Block block = new Block();
        for (NuthatchParser.StatementContext statCtx: ctx.statement())
            block.addChild(visit(statCtx));
        return block;
    }

    public JavaAstNode visitIfStatement(NuthatchParser.IfStatementContext ctx) {
        Expr condition = (Expr)visit(ctx.expression());
        Statement thenStatement = (Statement)visit(ctx.statement(0));
        Statement elseStatement = null;
        if (ctx.statement().size() == 2)
            elseStatement = (Statement)visit(ctx.statement(1));
        return new If(condition, thenStatement, elseStatement);
    }

    public JavaAstNode visitDeclarationStatement(NuthatchParser.DeclarationStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitAssignStatement(NuthatchParser.AssignStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitExpressionStatement(NuthatchParser.ExpressionStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitReturnStatement(NuthatchParser.ReturnStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitWalkToStatement(NuthatchParser.WalkToStatementContext ctx) {
        return new Return((Expr)visit(ctx.expression()));
    }

    public JavaAstNode visitStopStatement(NuthatchParser.StopStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitSuspendStatement(NuthatchParser.SuspendStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitFunctionStatement(NuthatchParser.FunctionStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitActionStatement(NuthatchParser.ActionStatementContext ctx) {
        return null;
    }

    public JavaAstNode visitReplaceStatement(NuthatchParser.ReplaceStatementContext ctx) {
        return new Apply(new Ref(new Id(WALKER_PARAM_NAME)), new Id("replace"), (Expr)visit(ctx.term()));
    }

    public JavaAstNode visitDeclaration(NuthatchParser.DeclarationContext ctx) {
        String nuthatchType = ctx.type().getText();
        String javaType = "String";  // Default variable type is String
        if (nuthatchType != null && !nuthatchType.isEmpty())
            javaType = capitalize(nuthatchType);
        return new Field(new Modifier("private"), new Type(null, javaType),
                new Id(ctx.ID().getText()), (Expr)visit(ctx.expression()));
    }

    public JavaAstNode visitBranchExpression(NuthatchParser.BranchExpressionContext ctx) {
        return visit(ctx.branch());
    }

    public JavaAstNode visitJoinPointExpression(NuthatchParser.JoinPointExpressionContext ctx) {
        return visit(ctx.joinpoint());
    }

    public JavaAstNode visitGetterExpression(NuthatchParser.GetterExpressionContext ctx) {
        return null;
    }

    public JavaAstNode visitNestedWalkExpression(NuthatchParser.NestedWalkExpressionContext ctx) {
        return null;
    }

    public JavaAstNode visitLiteralExpression(NuthatchParser.LiteralExpressionContext ctx) {
        return visit(ctx.literal());
    }

    public JavaAstNode visitRefExpression(NuthatchParser.RefExpressionContext ctx) {
        return new Apply(null, new Id("var"), new StringLiteral(ctx.ID().getText()));
    }

    public JavaAstNode visitIntLiteral(NuthatchParser.IntLiteralContext ctx) {
        return new IntLiteral(Integer.valueOf(ctx.INT().getText()));
    }

    public JavaAstNode visitStringLiteral(NuthatchParser.StringLiteralContext ctx) {
        return null;
    }

    public JavaAstNode visitParentBranch(NuthatchParser.ParentBranchContext ctx) {
        return null;
    }

    public JavaAstNode visitFirstBranch(NuthatchParser.FirstBranchContext ctx) {
        return null;
    }

    public JavaAstNode visitLastBranch(NuthatchParser.LastBranchContext ctx) {
        return null;
    }

    public JavaAstNode visitNextBranch(NuthatchParser.NextBranchContext ctx) {
        return new Ref(new Id("NEXT"));
    }

    public JavaAstNode visitProceedBranch(NuthatchParser.ProceedBranchContext ctx) {
        return null;
    }

    public JavaAstNode visitStayBranch(NuthatchParser.StayBranchContext ctx) {
        return null;
    }

    public JavaAstNode visitUpJoinPoint(NuthatchParser.UpJoinPointContext ctx) {
        return new Apply(null, new Id("up"), new Ref(new Id(WALKER_PARAM_NAME)));
    }

    public JavaAstNode visitDownJoinPoint(NuthatchParser.DownJoinPointContext ctx) {
        return new Apply(null, new Id("down"), new Ref(new Id(WALKER_PARAM_NAME)));
    }

    public JavaAstNode visitRootJoinPoint(NuthatchParser.RootJoinPointContext ctx) {
        return new Apply(null, new Id("root"), new Ref(new Id(WALKER_PARAM_NAME)));
    }

    public JavaAstNode visitLeafJoinPoint(NuthatchParser.LeafJoinPointContext ctx) {
        return new Apply(null, new Id("leaf"), new Ref(new Id(WALKER_PARAM_NAME)));
    }

    public JavaAstNode visitMatchAndBindJoinPoint(NuthatchParser.MatchAndBindJoinPointContext ctx) {
        Apply term = (Apply)visit(ctx.term());
        return new Apply(new Ref(new Id(WALKER_PARAM_NAME)), new Id("match"), term);
    }

    public JavaAstNode visitBeforeJoinPoint(NuthatchParser.BeforeJoinPointContext ctx) {
        return null;
    }

    public JavaAstNode visitAfterJoinPoint(NuthatchParser.AfterJoinPointContext ctx) {
        return null;
    }

    public JavaAstNode visitAction(NuthatchParser.ActionContext ctx) {
        return null;
    }

    public JavaAstNode visitFunction(NuthatchParser.FunctionContext ctx) {
        return null;
    }

    public JavaAstNode visitType(NuthatchParser.TypeContext ctx) {
        return null;
    }

    public JavaAstNode visitRefTerm(NuthatchParser.RefTermContext ctx) {
        // E.g. Add(x, 0) or x
        Arguments args = null;
        if (ctx.arguments() != null)
            args = (Arguments)visit(ctx.arguments());
        String termName = ctx.ID().getText();

        // Use var("x") instead of x
        if (args == null || args.getChildren().isEmpty())
            return new Apply(null, new Id("var"), new StringLiteral(termName));

        // E.g. Add(x, 0)
        Apply apply = new Apply(null, new Id(termName));
        for (Expr e : args.getExpressions())
            apply.addChild(e);
        return apply;
    }

    public JavaAstNode visitLiteralTerm(NuthatchParser.LiteralTermContext ctx) {
        return visit(ctx.literal());
    }

    public JavaAstNode visitArguments(NuthatchParser.ArgumentsContext ctx) {
        Arguments args = new Arguments();
        for (NuthatchParser.ExpressionContext exprCtx : ctx.expression())
            args.addChild(visit(exprCtx));
        return args;
    }

    public JavaAstNode visitArityGetter(NuthatchParser.ArityGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitRootGetter(NuthatchParser.RootGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitLeafGetter(NuthatchParser.LeafGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitNameGetter(NuthatchParser.NameGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitTypeGetter(NuthatchParser.TypeGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitDataGetter(NuthatchParser.DataGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitChildGetter(NuthatchParser.ChildGetterContext ctx) {
        return null;
    }

    public JavaAstNode visitLabelGetter(NuthatchParser.LabelGetterContext ctx) {
        return null;
    }

    private String capitalize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    private class Params extends JavaAstNode {
    }

    private class Arguments extends JavaAstNode {
        List<Expr> getExpressions() {
            return children.stream()
                    .map(e -> (Expr) e)
                    .collect(Collectors.toList());
        }
    }
}
