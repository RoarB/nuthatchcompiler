package nuthatch.java_ast;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Class extends JavaAstNode implements Statement {
    public Class(Modifier modifier, Id name, Id classExtends, Id classImplements) {
        addChild(modifier);
        addChild(name);
        addChild(classExtends);
        addChild(classImplements);
    }

    public Class(Modifier modifier, Id name, Id classExtends, Id classImplements, JavaAstNode children) {
        this(modifier, name, classExtends, classImplements);
        Collections.addAll(this.children, children);
    }

    public Modifier getModifier() {
        return (Modifier)children.get(0);
    }

    public Id getName() {
        return (Id)children.get(1);
    }

    public Id getExtends() {
        return (Id)children.get(2);
    }

    public Id getImplements() {
        return (Id)children.get(3);
    }

    public List<Field> getFields() {
        return children.stream()
                .filter(n -> n instanceof Field)
                .map(n -> (Field) n)
                .collect(Collectors.toList());
    }

    public List<Method> getMethods() {
        return children.stream()
                .filter(n -> n instanceof Method)
                .map(n -> (Method) n)
                .collect(Collectors.toList());
    }
}
