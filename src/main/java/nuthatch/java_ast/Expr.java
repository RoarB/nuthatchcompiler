package nuthatch.java_ast;

/**
 * Interface used for marking a node type as an expression
 */
public interface Expr extends AstNode {

}
