package nuthatch.java_ast;

public class If extends JavaAstNode implements Statement {
    public If(Expr cond, Statement s1, Statement s2) {
        addChild(cond);
        addChild(s1);
        addChild(s2);
    }

    public Expr getCondition() { return (Expr)children.get(0); }

    public Statement getThenStatement() { return (Statement) children.get(1); }

    public Statement getElseStatement() { return (Statement) children.get(2); }

    public String toString() {
        return "If(" + getCondition().toString() + ",\n   " + getThenStatement().toString() + ",\n   " + getElseStatement() + "\n)\n";
    }
}
